define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('CoursesCtrl', ['$rootScope', '$scope', 'Courses', function ($rootScope, $scope, Courses) {
        $scope.title = "Курсы";
        Courses.getAll();
        $scope.$watch('$root.courses', function (newVal) {
            $scope.gridOptions.data = newVal;
        });
        $scope.gridOptions = {
            paginationPageSizes: [10, 15],
            paginationPageSize: 10,
            enableRowSelection: true,
            enableSelectAll: false,
            multiSelect: false,
            columnDefs: [
                {field: 'Id', displayName: 'Идентификатор', enableCellEdit: false},
                {field: 'Name', displayName: 'Наименование'}
            ]
        };
        $scope.gridOptions.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (newValue !== oldValue) {
                    if (rowEntity.Id == 0) {
                        Courses.createEntity(rowEntity);
                    } else {
                        Courses.updateEntity(rowEntity);
                    }
                }
            });
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                if (row.isSelected) {
                    $scope.isRowSelected = true;
                    $scope.selectedRow = row.entity;
                } else {
                    $scope.isRowSelected = false;
                    $scope.selectedRow = null;
                }
            });

        };

        $scope.addRow = function () {
            $scope.gridOptions.data.push({Name: "", Id: 0});
            $scope.gridApi.pagination.seek($scope.gridApi.pagination.getTotalPages());
        };
        $scope.deleteRow = function () {
            if ($scope.isRowSelected) {
                var entity = $scope.selectedRow;
                Courses.deleteEntity(entity);
            }
        };
    }]);
});