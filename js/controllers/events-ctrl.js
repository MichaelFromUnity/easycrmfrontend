/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('EventsCtrl', ['$rootScope', '$scope', 'Clients', 'Courses','Events', function ($rootScope, $scope,  Clients, Courses, Events) {
        $scope.title = "Мероприятия";
        Clients.getAll();
        Courses.getAll();
        Events.getAll();
        $scope.$watch('$root.courses', function (newVal) {
            $scope.gridOptions.columnDefs[2].editDropdownOptionsArray = newVal;
        });
        $scope.$watch('$root.clients', function (newVal) {
            $scope.gridOptions.columnDefs[1].editDropdownOptionsArray = newVal;
        });
        $scope.$watch('$root.events', function (newVal) {
            $scope.gridOptions.data = newVal;
        });
        $scope.gridOptions = {
            paginationPageSizes: [10, 15],
            paginationPageSize: 10,
            enableRowSelection: true,
            enableSelectAll: false,
            multiSelect: false,
            columnDefs: [
                {field: 'Id', displayName: 'Идентификатор', enableCellEdit: false},
                {
                    field: 'ClientId',
                    displayName: 'Клиент',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownValueLabel: 'FullName',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: [],
                    cellFilter: 'clientFilter'
                },
                {
                    field: 'CourseId', displayName: 'Курс',
                    editDropdownValueLabel: 'Name',
                    editableCellTemplate: 'ui-grid/dropdownEditor',
                    editDropdownIdLabel: 'Id',
                    editDropdownOptionsArray: [],
                    cellFilter: 'courseFilter'

                }
            ]
        };
        $scope.gridOptions.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (newValue !== oldValue && rowEntity.ClientId > 0 && rowEntity.CourseId > 0) {
                    if (rowEntity.Id == 0) {
                        Events.createEntity(rowEntity);
                    } else {
                        Events.updateEntity(rowEntity);
                    }
                }
            });
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                if (row.isSelected) {
                    $scope.isRowSelected = true;
                    $scope.selectedRow = row.entity;
                } else {
                    $scope.isRowSelected = false;
                    $scope.selectedRow = null;
                }
            });

        };

        $scope.addRow = function () {
            $scope.gridOptions.data.push({Id: 0, ClientId: "", CourseId: ""});
            $scope.gridApi.pagination.seek($scope.gridApi.pagination.getTotalPages());
        };
        $scope.deleteRow = function () {
            if ($scope.isRowSelected) {
                var entity = $scope.selectedRow;
                Events.deleteEntity(entity);
            }
        };
    }]);
});