define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('ClientsCtrl', ['$rootScope', '$scope', 'Clients', function ($rootScope, $scope, Clients) {
        $scope.title = "Клиенты";
        Clients.getAll();
        $scope.$watch('$root.clients', function (newVal) {
            $scope.gridOptions.data = newVal;
        });
        $scope.gridOptions = {
            paginationPageSizes: [10, 15],
            paginationPageSize: 10,
            enableRowSelection: true,
            enableSelectAll: false,
            multiSelect: false,
            columnDefs: [
                {field: 'Id', displayName: 'Идентификатор', enableCellEdit: false},
                {field: 'FullName', displayName: 'Полное имя'}
            ]
        };
        $scope.gridOptions.onRegisterApi = function (gridApi) {
            //set gridApi on scope
            $scope.gridApi = gridApi;
            gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                if (newValue !== oldValue) {
                    if (rowEntity.Id == 0) {
                        Clients.createEntity(rowEntity);
                    } else {
                        Clients.updateEntity(rowEntity);
                    }
                }
            });
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                if (row.isSelected) {
                    $scope.isRowSelected = true;
                    $scope.selectedRow = row.entity;
                } else {
                    $scope.isRowSelected = false;
                    $scope.selectedRow = null;
                }
            });

        };

        $scope.addRow = function () {
            $scope.gridOptions.data.push({FullName: "", Id: 0});
            $scope.gridApi.pagination.seek($scope.gridApi.pagination.getTotalPages());
        };
        $scope.deleteRow = function () {
            if ($scope.isRowSelected) {
                var entity = $scope.selectedRow;
                Clients.deleteEntity(entity);
            }
        };
    }]);
});