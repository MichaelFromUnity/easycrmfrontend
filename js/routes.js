/**
 * Defines the main routes in the application.
 * The routes you see here will be anchors '#/' unless specifically configured otherwise.
 */

define(['./app'], function (app) {
    'use strict';
    return app.config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: '/partials/clients.html',
            controller: 'ClientsCtrl'
        });
        $routeProvider.when('/courses',{
            templateUrl: '/partials/courses.html',
            controller: 'CoursesCtrl'
        });
        $routeProvider.when('/events', {
            templateUrl: '/partials/events.html',
            controller: 'EventsCtrl'
        });
        $routeProvider.otherwise({
            redirectTo: '/'
        });
    }]);
});
