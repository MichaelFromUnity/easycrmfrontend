/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (services) {
    'use strict';
    services.factory('Events', ['$rootScope', 'Hub', '$timeout', function ($rootScope, Hub, $timeout) {
        $rootScope.events = [];
        var hub = new Hub('EventHub', {
            useSharedConnection : false,
            listeners: {
                'eventsUpdated': function (events) {
                    console.log("Handling eventsUpdated call from EventHub.");
                    $rootScope.events = events;
                    $rootScope.$apply();
                }
            },

            //server side methods
            methods: ['CreateEntity', 'DeleteEntity', 'UpdateEntity', 'GetAll'],

            //handle connection error
            errorHandler: function (error) {
                console.error(error);
            },


            rootPath: SIGNALR_URL,

            stateChanged: function (state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log("Connecting to EventHub...");
                        break;
                    case $.signalR.connectionState.connected:
                        console.log("Connected to EventHub.");
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log("Reconnecting to EventHub...");
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log("Disconnected from EventHub...");
                        break;
                }
            }
        });

        var CreateEntity = function (event) {
            hub.connection.start().done(function () {
                console.log("Calling CreateEntity method on EventHub");
                hub.CreateEntity(event)
            });
        };
        var DeleteEntity = function (event) {
            hub.connection.start().done(function () {
                console.log("Calling DeleteEntity method on EventHub");
                hub.DeleteEntity(event);
            });
        };
        var GetAll = function () {
            hub.connection.start().done(function () {
                console.log("Calling GetAll method on EventHub");
                hub.GetAll();
            });
        };
        var UpdateEntity = function (event) {
            hub.connection.start().done(function () {
                console.log("Calling UpdateEntity method on EventHub");
                hub.UpdateEntity(event);
            });
        };

        return {
            createEntity: CreateEntity,
            deleteEntity: DeleteEntity,
            updateEntity: UpdateEntity,
            getAll: GetAll
        };
    }]);
});