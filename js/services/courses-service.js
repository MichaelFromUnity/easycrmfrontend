/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (services) {
    'use strict';
    services.factory('Courses', ['$rootScope', 'Hub', '$timeout', function ($rootScope, Hub, $timeout) {
        $rootScope.courses = [];
        var hub = new Hub('CourseHub', {
            useSharedConnection : false,
            listeners: {
                'coursesUpdated': function (courses) {
                    console.log("Handling coursesUpdated call from CourseHub.");
                    $rootScope.courses = courses;
                    $rootScope.$apply();
                }
            },

            //server side methods
            methods: ['CreateEntity', 'DeleteEntity', 'UpdateEntity', 'GetAll'],

            //handle connection error
            errorHandler: function (error) {
                console.error(error);
            },


            rootPath: SIGNALR_URL,

            stateChanged: function (state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log("Connecting to CourseHub...");
                        break;
                    case $.signalR.connectionState.connected:
                        console.log("Connected to CourseHub.");
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log("Reconnecting to CourseHub...");
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log("Disconnected from CourseHub...");
                        break;
                }
            }
        });

        var CreateEntity = function (course) {
            hub.connection.start().done(function () {
                console.log("Calling CreateEntity method on CourseHub");
                hub.CreateEntity(course)
            });
        };
        var DeleteEntity = function (course) {
            hub.connection.start().done(function () {
                console.log("Calling DeleteEntity method on CourseHub");
                hub.DeleteEntity(course);
            });
        };
        var GetAll = function () {
            hub.connection.start().done(function () {
                console.log("Calling GetAll method on CourseHub");
                hub.GetAll();
            });
        };
        var UpdateEntity = function (course) {
            hub.connection.start().done(function () {
                console.log("Calling UpdateEntity method on CourseHub");
                hub.UpdateEntity(course);
            });
        };

        return {
            createEntity: CreateEntity,
            deleteEntity: DeleteEntity,
            updateEntity: UpdateEntity,
            getAll: GetAll
        };
    }]);
});