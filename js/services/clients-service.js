/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (services) {
    'use strict';
    services.factory('Clients', ['$rootScope', 'Hub', '$timeout', function ($rootScope, Hub, $timeout) {
        $rootScope.clients = [];
        var hub = new Hub('ClientHub', {
            useSharedConnection : false,
            listeners: {
                'clientsUpdated': function (clients) {
                    console.log("Handling clientsUpdated call from ClientHub.");
                    $rootScope.clients = clients;
                    $rootScope.$apply();
                }
            },

            //server side methods
            methods: ['CreateEntity', 'DeleteEntity', 'UpdateEntity', 'GetAll'],

            //handle connection error
            errorHandler: function (error) {
                console.error(error);
            },


            rootPath: SIGNALR_URL,

            stateChanged: function (state) {
                switch (state.newState) {
                    case $.signalR.connectionState.connecting:
                        console.log("Connecting to ClientHub...");
                        break;
                    case $.signalR.connectionState.connected:
                        console.log("Connected to ClientHub.");
                        break;
                    case $.signalR.connectionState.reconnecting:
                        console.log("Reconnecting to ClientHub...");
                        break;
                    case $.signalR.connectionState.disconnected:
                        console.log("Disconnected from ClientHub...");
                        break;
                }
            }
        });

        var CreateEntity = function (client) {
           hub.connection.start().done(function () {
               console.log("Calling CreateEntity method on ClientHub");
               hub.CreateEntity(client)
            });
        };
        var DeleteEntity = function (client) {
            hub.connection.start().done(function () {
                console.log("Calling DeleteEntity method on ClientHub");
                hub.DeleteEntity(client);
            });
        };
        var GetAll = function () {
            hub.connection.start().done(function () {
                console.log("Calling GetAll method on ClientHub");
                hub.GetAll();
            });
        };
        var UpdateEntity = function (client) {
            hub.connection.start().done(function () {
                console.log("Calling UpdateEntity method on ClientHub");
                hub.UpdateEntity(client);
            });
        };

        return {
            createEntity: CreateEntity,
            deleteEntity: DeleteEntity,
            updateEntity: UpdateEntity,
            getAll: GetAll
        };
    }]);
});