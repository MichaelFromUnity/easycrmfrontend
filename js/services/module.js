/**
 * Created by Binali on 27.02.2015.
 */
define(['angular'], function (ng) {
    'use strict';
    return ng.module('app.services', []);
});
