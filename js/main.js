/**
 * configure RequireJS
 * prefer named modules to long paths, especially for version mgt
 * or 3rd party libraries
 */

const SIGNALR_URL = 'http://localhost:6700/signalr';

require.config({
    baseUrl: './',
    paths: {
        'angular': 'lib/angular/angular',
        'angular-route': 'lib/angular-route/angular-route',
        'domReady': 'lib/requirejs-domready/domReady',
        'angular-cookies': 'lib/angular-cookies/angular-cookies',
        'angular-resource': 'lib/angular-resource/angular-resource',
        'angular-strap': 'lib/angular-strap/dist/angular-strap',
        'angular-animate' : 'lib/angular-animate/angular-animate',
        'angular-strap.tpl': 'lib/angular-strap/dist/angular-strap.tpl',
        'jquery-signalR' : 'lib/signalr/jquery.signalR',
        'angular-signalr-hub': 'lib/angular-signalr-hub/signalr-hub',
        'jquery': 'lib/jquery/dist/jquery',
        'signalr-hubs': SIGNALR_URL + '/hubs?',
        'ng-grid': 'https://cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min'
    },

    /**
     * for libs that either do not support AMD out of the box, or
     * require some fine tuning to dependency mgt'
     */
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ng-grid': {
            exports: 'ng-grid',
            deps:['angular']
        },
        'jquery': {
            export: 'jquery'
        },
        'angular-route': {
            deps: ['angular']
        },
        'angular-cookies': {
            deps: ['angular']
        },
        'angular-resource': {
            deps: ['angular']
        },
        'angular-strap': {
            deps: ['angular']
        },
        'angular-animate': {
            deps: ['angular']
        },
        'angular-strap.tpl': {
            deps:['angular-strap']
        },
        'jquery-signalR': {
          export: 'jquery-signalR',
           deps: ['jquery']
        },
        'signalr-hubs': {
            export: 'signalr-hubs',
            deps: ['jquery-signalR']
        },
        'angular-signalr-hub': {
            export : 'angular-signalr-hub',
            deps: ['signalr-hubs', 'angular']
        }
    },

    deps: [
        // kick start application... see bootstrap.js
        'js/bootstrap'
    ]
});
