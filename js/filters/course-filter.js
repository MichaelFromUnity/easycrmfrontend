/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (filters) {
    'use strict';
    filters.filter('courseFilter', ['$rootScope', function ($rootScope) {
        return function (value) {
            var res = $rootScope.courses.filter(function (c) {
                return c.Id == value;
            });
            if (res != undefined && res.length > 0)
                return res[0].Name;
            else
                return '';
        };
    }]);
});