/**
 * Created by Бинали on 04.08.2016.
 */
define(['angular'], function (ng) {
    'use strict';
    return ng.module('app.filters', []);
});