/**
 * Created by Бинали on 04.08.2016.
 */
define(['./module'], function (filters) {
    'use strict';
    filters.filter('clientFilter', ['$rootScope', function ($rootScope) {
        return function (value) {
            var res = $rootScope.clients.filter(function (c) {
                return c.Id == value;
            });
            if (res != undefined && res.length > 0)
                return res[0].FullName;
            else
                return '';
        };
    }]);
});